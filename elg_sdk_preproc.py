#!/usr/bin/env python3
from estnltk import Text
from estnltk.taggers import SentenceTokenizer

from elg import FlaskService
from elg.model import StructuredTextRequest
from elg.model import TextsResponse

import github_tts_preprocess_et.convert as tts_preproc_et

'''
# command line script
python3 -m venv venv_elg_estnltk
venv_elg_estnltk/bin/python3 -m pip install --upgrade pip
venv_elg_estnltk/bin/pip3 --no-cache-dir install -r requirements.txt

venv_elg_estnltk/bin/python3 ./elg_sdk_preproc.py --json='{"type":"structuredText", "texts":[{"content":"1. lause"},{"content":"2. lause"}]}'

# web server in docker & curl
docker build -t tilluteenused/tartunlp_tts_preproc_et .
docker run -p 8000:8000 tilluteenused/tartunlp_tts_preproc_et
curl -i --request POST --header "Content-Type: application/json" --data '{"type":"structuredText", "texts":[{"content":"1. lause"},{"content":"AS Sarved&Sõrad"}]}' localhost:8000/process
curl -i --request POST --header "Content-Type: application/json" --data '{"type":"text","content":"1. lause. AS Sarved&Sõrad"}' localhost:8000/process
docker login -u tilluteenused
docker push tilluteenused/tartunlp_tts_preproc_et

# some links
* https://european-language-grid.readthedocs.io/en/latest/all/A2_API/LTPublicAPI.html
* https://european-language-grid.readthedocs.io/en/release1.1.1/all/A2_API/LTInternalAPI.html
'''


class TartuNLP_tts_preproc_et(FlaskService):
    def process_text(self, request) -> TextsResponse:
        '''
        Find sentences and tokens
        :param content: {TextRequest} - input text in ELG TextsResponse format
        :return: {TextsResponse} -  Converted sentences in ELG- format
        '''
        estnltk_text = Text(request.content)
        estnltk_text.tag_layer(['words'])
        SentenceTokenizer().tag(estnltk_text)
        texts_out = []
        for sentence_in in estnltk_text.sentences:
            texts_out.append({"content": tts_preproc_et.convert_sentence(sentence_in.enclosing_text)})
        return TextsResponse(texts=texts_out)


    def process_structured_text(self, request: StructuredTextRequest) -> TextsResponse:
        texts_out = []
        for text_in in request.texts:
            texts_out.append({"content": tts_preproc_et.convert_sentence(text_in.content)})
        return TextsResponse(texts=texts_out)


flask_service = TartuNLP_tts_preproc_et("TartuNLP_tts_preproc_et")
app = flask_service.app


def run_test(my_query_str: str) -> None:
    '''
    Run as command line script
    :param my_query_str: input in json string
    '''
    my_query = json.loads(my_query_str)
    service = TartuNLP_tts_preproc_et("TartuNLP tts preproc et")
    request = StructuredTextRequest(texts=my_query["texts"])
    response = service.process_structured_text(request)

    response_json_str = response.json(exclude_unset=True)  # exclude_none=True
    response_json_json = json.loads(response_json_str)
    return response_json_json


def run_server() -> None:
    '''
    Run as flask webserver
    '''
    app.run()


if __name__ == '__main__':
    import argparse
    import json
    import sys
    argparser = argparse.ArgumentParser(allow_abbrev=False)
    argparser.add_argument('-j', '--json', type=str, help='sentences to precrocess')
    args = argparser.parse_args()
    if args.json is None:
        run_server()
    else:
        '''
        query_json = \
            {
                "type":"structuredText",
                "texts":[
                    {"content":"1. lause"},
                    {"content":"2. lause"}
                ]
            }
        query_text=json.dumps(query_json)
        json.dump(run_test(query_text), sys.stdout, indent=4)
        '''
        json.dump(run_test(args.json), sys.stdout, indent=4)

