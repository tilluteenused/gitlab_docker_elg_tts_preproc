## Preprocessing script for Estonian text-to-speech applications

Container (docker) of 
[preprocessing script for Estonian text-to-speech applications](https://github.com/TartuNLP/tts_preprocess_et) with interface compliant with 
[ELG requirements](https://european-language-grid.readthedocs.io/en/release1.0.0/all/LTInternalAPI.html#).

## Contains  <a name="Contains"></a>

* [TartuNLP preprocessing script for text-to-speech applications](https://github.com/TartuNLP/tts_preprocess_et)
* Container and interface code

## Preliminaries

* You should have software for making / using the container installed; see instructions on the [docker web site](https://docs.docker.com/).
* In case you want to compile the code or build the container yourself, you should have version control software installed; see instructions on the [git web site](https://git-scm.com/).

## Downloading image from Docker Hub

You may dowload a ready-made container from Docker Hub, using the Linux command line (Windows / Mac command lines are similar):

```commandline
docker pull tilluteenused/tartunlp_tts_preproc_et:1.0.0
```

Next, continue to the section [Starting the container](#Starting_the_container).

## Making your own container

### 1. Downloading the source code

The source code comes from two sources.

#### 1.1  Downloading [container and interface code](https://gitlab.com/tilluteenused/gitlab_docker_elg_tts_preproc)

```commandline
mkdir -p ~/gitlab-docker-elg
cd ~/gitlab-docker-elg
git clone https://gitlab.com/tilluteenused/gitlab_docker_elg_tts_preproc.git gitlab_docker_elg_tts_preproc
```

#### 1.2 Downloading [preprocessing script code](https://github.com/TartuNLP/tts_preprocess_et)

```commandline
cd gitlab_docker_elg_tts_preproc
git clone https://github.com/TartuNLP/tts_preprocess_et.git github_tts_preprocess_et
```

### 2. Building the container

```commandline
cd ~/gitlab-docker-elg/gitlab_docker_elg_tts_preproc
docker build -t tilluteenused/tartunlp_tts_preproc_et:1.0.0 .
```

## Starting the container <a name="Starting the container"></a>

```commandline
docker run -p 8000:8000 tilluteenused/tartunlp_tts_preproc_et:1.0.0
```

One need not be in a specific directory to start the container.

Ctrl+C in a terminal window with a running container in it will terminate the container.

## Query json

### Version 1. 

Input is plain text.

```json
{
  "type":"text",
  "content": string, /* "The text of the request" */
  "params":{...}     /* optional */
}
```

### Version 2.

Input is an array of sentences.

```json
{
  "type": "structuredText",
  "texts": [              /* array of sentences */
    {
      "content": string   /* sentence */
    }
  ]
}
```

## Response json

Response json is the same for both query versions. 

```json
{
  "response":{
    "type":"texts",
    "texts":[              /* array of sentences */
      {
        "content": string  /* modified sentence */
      }
    ]
  }
}
```

## Usage examples

### Example 1.

Input is plain text.

```commandline
curl -i --request POST --header "Content-Type: application/json" --data '{"type":"text","content":"1. lause. AS Sarved&Sõrad"}' localhost:8000/process
```

```json
HTTP/1.1 200 OK
Server: gunicorn
Date: Sun, 20 Feb 2022 21:21:16 GMT
Connection: close
Content-Type: application/json
Content-Length: 111

{"response":{"type":"texts","texts":[{"content":"Esimene lause."},{"content":"aa-ess Sarved ja S\u00f5rad"}]}}
```

### Example 2. 

Input is an array of sentences.

```commandline
curl -i --request POST --header "Content-Type: application/json" --data '{"type":"structuredText", "texts":[{"content":"1. lause"},{"content":"AS Sarved&Sõrad"}]}' localhost:8000/process
```

```json
HTTP/1.1 200 OK
Server: gunicorn
Date: Sun, 20 Feb 2022 21:20:54 GMT
Connection: close
Content-Type: application/json
Content-Length: 110

{"response":{"type":"texts","texts":[{"content":"Esimene lause"},{"content":"aa-ess Sarved ja S\u00f5rad"}]}}
```

## Sponsors

The container development was sponsored by EU CEF project [Microservices at your service](https://www.lingsoft.fi/en/microservices-at-your-service-bridging-gap-between-nlp-research-and-industry)


## Authors

Authors of the container: Tarmo Vaino, Heiki-Jaan Kaalep

Authors of the contents of the container: see references at section [Contains](#Contains).
 
