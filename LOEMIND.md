## TartuNLP sünteeskõne eeltöötlusmoodul Docker'i konteineris

[TartuNLP sünteeskõne eeltöötlusmoodulil](https://github.com/TartuNLP/tts_preprocess_et) põhinev tarkvara-konteiner (docker),
mille liides vastab [ELG nõuetele](https://european-language-grid.readthedocs.io/en/release1.0.0/all/LTInternalAPI.html#).

## Mida sisaldab <a name="Mida_sisaldab"></a>

* [TartuNLP sünteeskõne eeltöötlusmoodul](https://github.com/TartuNLP/tts_preprocess_et)
* Konteineri ja liidesega seotud lähtekood

## Eeltingimused

* Peab olema paigaldatud tarkvara konteineri tegemiseks/kasutamiseks; juhised on [docker'i veebilehel](https://docs.docker.com/).
* Kui sooviks on lähtekoodi ise kompileerida või konteinerit kokku panna, siis peab olema paigaldatud versioonihaldustarkvara; juhised on [git'i veebilehel](https://git-scm.com/).

## Konteineri allalaadimine Docker Hub'ist

Valmis konteineri saab laadida alla Docker Hub'ist, kasutades Linux'i käsurida (Windows'i/Mac'i käsurida on analoogiline):

```commandline
docker pull tilluteenused/tartunlp_tts_preproc_et:1.0.0
```

Seejärel saab jätkata osaga [Konteineri käivitamine](#Konteineri_käivitamine).

## Ise konteineri tegemine

### 1. Lähtekoodi allalaadimine

Lähtekood tuleb laadida alla kahest kohast.

#### 1.1  [Konteineri ja liidesega seotud lähtekoodi](https://gitlab.com/tilluteenused/gitlab_docker_elg_tts_preproc) allalaadimine

```commandline
mkdir -p ~/gitlab-docker-elg
cd ~/gitlab-docker-elg
git clone https://gitlab.com/tilluteenused/gitlab_docker_elg_tts_preproc.git gitlab_docker_elg_tts_preproc
```

#### 1.2 [Eeltöötlusmooduli lähtekoodi](https://github.com/TartuNLP/tts_preprocess_et) allalaadimine

```commandline
cd gitlab_docker_elg_tts_preproc
git clone https://github.com/TartuNLP/tts_preprocess_et.git github_tts_preprocess_et
```

### 2. Konteineri kokkupanemine 

```commandline
cd ~/gitlab-docker-elg/gitlab_docker_elg_tts_preproc
docker build -t tilluteenused/tartunlp_tts_preproc_et:1.0.0 .
```

## Konteineri käivitamine <a name="Konteineri_käivitamine"></a>

```commandline
docker run -p 8000:8000 tilluteenused/tartunlp_tts_preproc_et:1.0.0
```

Pole oluline, milline on jooksev kataloog terminaliaknas konteineri käivitamise hetkel.

Käivitatud konteineri töö lõpetab Ctrl+C selles terminaliaknas, kust konteiner käivitati.

## Päringu json-kuju

### Variant 1.

Sisendiks on lihttekst.

```json
{
  "type":"text",
  "content": string, /* "Töötlemist vajav tekst" */
  "params":{...}     /* võib puududa */
}
```

### Variant 2.

Sisendiks on lausestatud tekst.

```json
{
  "type": "structuredText",
  "texts": [            /* lausete massiiv */
    {
      "content": string /* lause */
    }
  ]
}
```

## Vastuse json-kuju

Vastuse kuju on mõlema variandi puhul ühesugune.

```json
{
  "response":{
    "type":"texts",
    "texts":[             /* lausete massiiv */
      {
        "content": string /* töödeldud sisendlause */
      }
    ]
  }
}
```

## Kasutusnäide

### Näide 1. 

Sisendiks on lihttekst.

```commandline
curl -i --request POST --header "Content-Type: application/json" --data '{"type":"text","content":"1. lause. AS Sarved&Sõrad"}' localhost:8000/process
```

```json
HTTP/1.1 200 OK
Server: gunicorn
Date: Sun, 20 Feb 2022 21:21:16 GMT
Connection: close
Content-Type: application/json
Content-Length: 111

{"response":{"type":"texts","texts":[{"content":"Esimene lause."},{"content":"aa-ess Sarved ja S\u00f5rad"}]}}
```

### Näide 2. 

Sisendiks on lausestatud tekst.

```commandline
curl -i --request POST --header "Content-Type: application/json" --data '{"type":"structuredText", "texts":[{"content":"1. lause"},{"content":"AS Sarved&Sõrad"}]}' localhost:8000/process
```

```json
HTTP/1.1 200 OK
Server: gunicorn
Date: Sun, 20 Feb 2022 21:20:54 GMT
Connection: close
Content-Type: application/json
Content-Length: 110

{"response":{"type":"texts","texts":[{"content":"Esimene lause"},{"content":"aa-ess Sarved ja S\u00f5rad"}]}}
```

## Rahastus

Konteiner loodi EL projekti [Microservices at your service](https://www.lingsoft.fi/en/microservices-at-your-service-bridging-gap-between-nlp-research-and-industry) toel.

## Autorid

Konteineri autorid: Tarmo Vaino, Heiki-Jaan Kaalep

Konteineri sisu autoreid vt. jaotises [Mida sisaldab](#Mida_sisaldab) toodud viidetest.
 
