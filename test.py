import sys
import json
import requests

'''
For testing tools:
    https://gitlab.com/tilluteenused/gitlab_docker_elg_tts_preproc
'''

if __name__ == '__main__':
    import argparse
    argparser = argparse.ArgumentParser(allow_abbrev=False)
    argparser.add_argument('-i', '--indent', action="store_true", required=False,
                           help='väljundisse taanetega json')
    args = argparser.parse_args()

    query_json = json.loads('{"type":"structuredText", "texts":[{"content":"1. lause"},{"content":"2. lause"}]}')
    resp = requests.post('http://localhost:8000/process', json=query_json)
    resp_text = resp.content.decode('utf-8')
    resp_json = json.loads(resp_text)

    if args.indent is True:
        json.dump(query_json, sys.stdout, indent=4)
        print('\n----------')
        json.dump(resp_json, sys.stdout, indent=4)
    else:
        json.dump(query_json, sys.stdout)
        print('\n----------')
        json.dump(resp_json, sys.stdout)
